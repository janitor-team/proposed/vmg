vmg (3.7.1-8) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

  [ Samuel Thibault ]
  * control: Make Multi-Arch: foreign.
  * source/lintian-overrides: Update dropping warnings.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 04 Apr 2022 11:46:42 -0000

vmg (3.7.1-7) unstable; urgency=medium

  * control: Bump Standards-Version to 4.6.0 (no change)
  * Play cat and mice again with lintian warning suppression.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 29 Jan 2022 14:41:39 +0100

vmg (3.7.1-6) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Set Rules-Requires-Root to no.
  * source/lintian-overrides: Update suppression.

  [ Frédéric Bonnard ]
  * rules: fix the path for fpc unit files (Closes: Bug#1000651)

 -- Samuel Thibault <sthibault@debian.org>  Fri, 26 Nov 2021 16:20:33 +0100

vmg (3.7.1-5) unstable; urgency=medium

  * source/lintian-overrides: Ignore warning about extra-long lines in source
    file.
  * control: Update alioth list domain.
  * control: Bump Standards-Version to 4.5.0 (no change)
  * control: Bump debhelper compat to 12.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 01 Nov 2020 02:11:08 +0100

vmg (3.7.1-4) unstable; urgency=medium

  * control: Migrate priority to optional.
  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.0.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 29 Aug 2019 00:57:31 +0200

vmg (3.7.1-3) unstable; urgency=medium

  [ Samuel Thibault ]
  * Use canonical anonscm vcs URL.
  * control: Update maintainer mailing list.

  [ Paul Gevers ]
  * Add Build-Depends on fp-units-misc to enable building with fpc 3.0.2
    and lazarus 1.8

 -- Paul Gevers <elbrus@debian.org>  Tue, 11 Jul 2017 20:40:44 +0200

vmg (3.7.1-2) unstable; urgency=medium

  * rules: hack DEB_HOST_GNU_CPU=i[56]86 into i386.
  * control: Drop lazarus build-depend, not actually needed.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 05 Jun 2016 02:13:49 +0200

vmg (3.7.1-1) unstable; urgency=low

  * Initial release. (Closes: Bug#825971)

 -- Samuel Thibault <sthibault@debian.org>  Fri, 03 Jun 2016 02:44:00 +0200
